package br.ucsal.bes20172.bd2.exerciciojpa01.load;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Administrativo;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Cidade;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Endereco;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Estado;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.PessoaJuridica;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.RamoAtividade;
import br.ucsal.bes20172.bd2.exerciciojpa01.domain.Vendedor;

public class Carga {

	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	public static void main(String[] args) throws ParseException {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("exerciciojpa01");

		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		Estado estado1 = new Estado("BA", "Bahia");
		Estado estado2 = new Estado("SP", "S�o Paulo");
		Estado estado3 = new Estado("PA", "Par�");

		em.persist(estado1);
		em.persist(estado2);
		em.persist(estado3);

		Cidade cidade1 = new Cidade("SSA", "Salvador", estado1);
		Cidade cidade2 = new Cidade("FSA", "Feira de Santana", estado1);
		Cidade cidade3 = new Cidade("VCA", "VIt�ria da Conquista", estado1);
		Cidade cidade4 = new Cidade("SPC", "S�o Paulo", estado2);
		Cidade cidade5 = new Cidade("SJC", "S�o Jos� dos Campos", estado2);

		em.persist(cidade1);
		em.persist(cidade2);
		em.persist(cidade3);
		em.persist(cidade4);
		em.persist(cidade5);

		RamoAtividade ramoAtividade1 = new RamoAtividade("Inform�tica");
		RamoAtividade ramoAtividade2 = new RamoAtividade("Alimentos");
		RamoAtividade ramoAtividade3 = new RamoAtividade("Turismo");

		em.persist(ramoAtividade1);
		em.persist(ramoAtividade2);
		em.persist(ramoAtividade3);

		List<RamoAtividade> ramosAtv1 = new ArrayList<>();
		List<RamoAtividade> ramosAtv2 = new ArrayList<>();

		ramosAtv1.add(ramoAtividade1);
		ramosAtv2.add(ramoAtividade3);

		PessoaJuridica pessoaJuridica1 = new PessoaJuridica("81.303.154/0001-54", "InfoTec", new BigDecimal(50000),
				ramosAtv1);
		PessoaJuridica pessoaJuridica2 = new PessoaJuridica("81.303.154/0001-12", "TecGlobal", new BigDecimal(1000000),
				ramosAtv1);
		PessoaJuridica pessoaJuridica3 = new PessoaJuridica("81.303.154/0001-35", "InfoZone", new BigDecimal(590000),
				ramosAtv1);
		PessoaJuridica pessoaJuridica4 = new PessoaJuridica("81.303.154/0001-99", "Viaje Aqui", new BigDecimal(750000),
				ramosAtv2);
		PessoaJuridica pessoaJuridica5 = new PessoaJuridica("81.303.154/0001-01", "Seu Destino",
				new BigDecimal(10050000), ramosAtv2);

		em.persist(pessoaJuridica1);
		em.persist(pessoaJuridica2);
		em.persist(pessoaJuridica3);
		em.persist(pessoaJuridica4);
		em.persist(pessoaJuridica5);

		List<PessoaJuridica> clientes1 = new ArrayList<>();
		List<PessoaJuridica> clientes2 = new ArrayList<>();

		clientes1.add(pessoaJuridica1);
		clientes1.add(pessoaJuridica2);
		clientes1.add(pessoaJuridica3);

		clientes2.add(pessoaJuridica4);
		clientes2.add(pessoaJuridica5);

		Endereco endereco1 = new Endereco("Rua de Salvador", "Bairro de Salvador", cidade1);
		Endereco endereco2 = new Endereco("Rua Bonita", "Lindo", cidade2);
		Endereco endereco3 = new Endereco("Rua Arruda", "Tancredo Neves", cidade1);
		Endereco endereco4 = new Endereco("Rua dos Anjos", "Barra", cidade2);

		Vendedor vendedor1 = new Vendedor("167.256.620-72", "Veronica", "711111111", sdf.parse("10/05/1995"), endereco1,
				new BigDecimal(0.5), clientes1);

		Vendedor vendedor2 = new Vendedor("167.256.620-66", "Fabio", "722222222", sdf.parse("10/05/1993"), endereco3,
				new BigDecimal(0.2), clientes1);
		Vendedor vendedor3 = new Vendedor("167.256.620-77", "Juca", "733333333", sdf.parse("10/05/1982"), endereco2,
				new BigDecimal(0.18), null);
		Vendedor vendedor4 = new Vendedor("167.256.620-55", "Vanessa", "744444444", sdf.parse("10/05/1985"), endereco2,
				new BigDecimal(0.4), clientes2);
		Vendedor vendedor5 = new Vendedor("167.256.620-01", "Amanda", "755555555", sdf.parse("10/05/1990"), endereco1,
				new BigDecimal(0.07), null);

		em.persist(vendedor1);
		em.persist(vendedor2);
		em.persist(vendedor3);
		em.persist(vendedor4);
		em.persist(vendedor5);

		Administrativo administrativo1 = new Administrativo("167.256.720-47", "Jo�o", "79999999",
				sdf.parse("10/03/1995"), endereco4, 1);
		Administrativo administrativo2 = new Administrativo("167.456.620-45", "Pedro", "78188818",
				sdf.parse("11/03/1995"), endereco3, 2);
		Administrativo administrativo3 = new Administrativo("157.246.520-97", "Carla", "795455588",
				sdf.parse("10/06/1995"), endereco2, 3);

		em.persist(administrativo1);
		em.persist(administrativo2);
		em.persist(administrativo3);

		List<Vendedor> vendedores1 = new ArrayList<>();
		List<Vendedor> vendedores2 = new ArrayList<>();

		vendedores1.add(vendedor2);
		vendedores1.add(vendedor1);

		pessoaJuridica1.setVendedores(vendedores1);
		pessoaJuridica3.setVendedores(vendedores2);

		TypedQuery<Estado> jpql = em.createQuery(
				"select e from Estado e left join Cidade c on e.sigla = c.estado where c.sigla is null", Estado.class);
		List<Estado> estadoResultado2 = jpql.getResultList();

		for (Estado estado : estadoResultado2) {
			System.out.println(estado.getNome());
		}

		em.getTransaction().commit();

		em.close();
		emf.close();

	}

}
