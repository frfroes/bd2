package br.ucsal.bes20172.bd2.exerciciojpa01.domain;

import java.util.Date;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import br.ucsal.bes20172.bd2.exerciciojpa01.validadores.GrupoAprovacaoValidador;

public class Exemplo {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("exerciciojpa01");

		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		Estado estado1 = new Estado();
		estado1.sigla = "BA";
		estado1.nome = "Bahia";
		em.persist(estado1);

		Cidade cidade1 = new Cidade();
		cidade1.sigla = "SSA";
		cidade1.nome = "Salvador";
		cidade1.estado = estado1;
		em.persist(cidade1);

		Administrativo administrativo1 = new Administrativo();
		administrativo1.cpf = "12345678910";
		administrativo1.nome = "claudio neiva";
		administrativo1.dataNascimento = new Date();
		administrativo1.telefone = "123123";
		administrativo1.endereco = new Endereco();
		administrativo1.endereco.bairro = "Bairro 1";
		administrativo1.endereco.logradouro = "Rua X";
		administrativo1.endereco.cidade = cidade1;
		administrativo1.turno = 1;
		em.persist(administrativo1);

		// em.persist(administrativo1);

		RamoAtividade ramoAtividade = new RamoAtividade();
		ramoAtividade.nome = "transporte";

		// Valida��o - INICIO

		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

		Validator validator = factory.getValidator();

		Set<ConstraintViolation<RamoAtividade>> constraints = validator.validate(ramoAtividade,
				GrupoAprovacaoValidador.class);

		System.out.println("******************");

		for (ConstraintViolation<RamoAtividade> constraint : constraints) {
			System.out.println(constraint.getPropertyPath() + "  " + constraint.getMessage());
		}

		// Valida��o - FIM

		if (constraints.size() == 0) {
			em.persist(ramoAtividade);
		}

		try {
			em.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("DEU ERRO NA ATUALIZACAO DO RAMO DE ATIVIDADE ");
		}

		em.clear();

		// Funcionario funcionario1 = em.find(Funcionario.class, "12345678910");

		// Vendedor vendedor1 = em.find(Vendedor.class, "123123");

		RamoAtividade ramoAtividade2 = em.find(RamoAtividade.class, 78L);

		// System.out.println("Estado:" +
		// funcionario1.endereco.cidade.estado.nome);

		emf.close();

	}

}
