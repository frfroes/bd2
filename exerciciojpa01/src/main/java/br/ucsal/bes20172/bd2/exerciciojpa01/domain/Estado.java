package br.ucsal.bes20172.bd2.exerciciojpa01.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Estado {

	public Estado(String sigla, String nome) {
		super();
		this.sigla = sigla;
		this.nome = nome;
	}

	public Estado() {
		super();
	}

	@Id
	@Column(length = 2)
	String sigla;

	@Column(length = 50)
	String nome;

	// N�o faz parte da especifica��o do exerc�cio, mas, para exemplificar a
	// rela��o bidirecional, vamos acrescentar a lista de cidades � classe
	// Estado.
	@OneToMany(mappedBy = "estado")
	List<Cidade> cidades;

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}

	
}
