package br.ucsal.bes20172.bd2.exerciciojpa01.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.PostLoad;

//import org.hibernate.annotations.ColumnDefault;

import br.ucsal.bes20172.bd2.exerciciojpa01.converters.SituacaoConverter;

@Entity
// @DiscriminatorValue("V")
public class Vendedor extends Funcionario {
	
	public Vendedor(){}

	public Vendedor(String cpf, String nome, String telefone, Date dataNascimento, Endereco endereco,
			BigDecimal percentualComissao, List<PessoaJuridica> clientes) {
		super(cpf, nome, telefone, dataNascimento, endereco);
		this.percentualComissao = percentualComissao;
		this.situacao = Situacao.ATIVO;
		this.clientes = clientes;
	}

	@Column(name = "percentual_comissao", nullable = false, columnDefinition = " numeric(10,2) check (percentual_comissao >=0) ")
	BigDecimal percentualComissao;

	@Convert(converter = SituacaoConverter.class)
	@Column(nullable = false, columnDefinition = " char(1) default 'A' ")
	// @ColumnDefault(value = "'A'") // N�o � interessante utilizar, pois amarra
	// o aplicativo ao hibernate.
	Situacao situacao;

	@ManyToMany(mappedBy = "vendedores", cascade=CascadeType.ALL)
	List<PessoaJuridica> clientes;

}
