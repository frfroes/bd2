package br.ucsal.bes20172.bd2.exerciciojpa01.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
// @DiscriminatorValue("A")
public class Administrativo extends Funcionario {
	
	public Administrativo(){}
	
	public Administrativo(String cpf, String nome, String telefone, Date dataNascimento, Endereco endereco,
			Integer turno) {
		super(cpf, nome, telefone, dataNascimento, endereco);
		this.turno = turno;
	}

	@Column(nullable = false)
	Integer turno;

}
