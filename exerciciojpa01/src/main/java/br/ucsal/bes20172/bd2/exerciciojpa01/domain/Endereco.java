package br.ucsal.bes20172.bd2.exerciciojpa01.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class Endereco {
	
	public Endereco(){}

	public Endereco(String logradouro, String bairro, Cidade cidade) {
		super();
		this.logradouro = logradouro;
		this.bairro = bairro;
		this.cidade = cidade;
	}

	@Column(length = 150, nullable = false)
	String logradouro;

	@Column(length = 50, nullable = false)
	String bairro;

	@ManyToOne(optional = false)
	Cidade cidade;
}
