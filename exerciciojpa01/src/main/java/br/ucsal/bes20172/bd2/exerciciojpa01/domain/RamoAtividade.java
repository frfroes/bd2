package br.ucsal.bes20172.bd2.exerciciojpa01.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

import br.ucsal.bes20172.bd2.exerciciojpa01.validadores.GrupoAprovacaoValidador;
import br.ucsal.bes20172.bd2.exerciciojpa01.validadores.GrupoPedidoValidador;

@Entity
@SequenceGenerator(name = "sequence_ramo_atividade", sequenceName = "sq_ramoatividade1", initialValue = 78, allocationSize = 1)
public class RamoAtividade {

	public RamoAtividade() {
		super();
	}

	public RamoAtividade(@NotEmpty String nome) {
		super();
		this.nome = nome;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_ramo_atividade")
	Long id;

	@Column(nullable = false, length = 40)
	@NotEmpty
	String nome;

//	@PostLoad
//	public void auditoriaConsulta() {
//		System.out.println("voc� consultou o ramo de atividade: " + nome);
//	}
//
//	@PrePersist
//	public void auditoriaPrePersistencia() {
//		System.out.println("voc� atualizou (PRE) o ramo de atividade: " + nome);
//	}
//
//	@PostPersist
//	public void auditoriaPosPersistencia() {
//		System.out.println("voc� atualizou (POS) o ramo de atividade: " + nome);
//	}
}
