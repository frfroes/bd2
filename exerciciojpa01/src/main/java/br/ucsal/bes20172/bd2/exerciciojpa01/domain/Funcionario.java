package br.ucsal.bes20172.bd2.exerciciojpa01.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

// @Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

// @Inheritance(strategy = InheritanceType.SINGLE_TABLE)
// @DiscriminatorColumn(name = "tipo_funcionario", length = 1)
// @DiscriminatorValue("F")

//@MappedSuperclass
//public abstract class Funcionario {

@Entity
@Table(name = "tab_funcionario")
@Inheritance(strategy = InheritanceType.JOINED)
public class Funcionario {
	
	public Funcionario(){}

	public Funcionario(String cpf, String nome, String telefone, Date dataNascimento, Endereco endereco) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.telefone = telefone;
		this.dataNascimento = dataNascimento;
		this.endereco = endereco;
	}

	@Id
	@Column(length = 14)
	String cpf;

	@Column(length = 40, nullable = false)
	String nome;

	@Column(length = 11, nullable = true)
	String telefone;

	@Column(name = "data_nascimento", nullable = false)
	@Temporal(TemporalType.DATE)
	Date dataNascimento;

	@Embedded
	Endereco endereco;

}
